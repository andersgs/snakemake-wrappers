'''
A Snakemake wrapper for Spades Assembler
'''
__author__ = "Anders Gonçalves da Silva"
__copyright__ = "Copyright 2018, Anders Gonçalves da Silva"
__email__ = "andersgs@gmail.com"
__license__ = "MIT"


import os
import shutil
import pathlib
from snakemake.shell import shell

log = snakemake.log_fmt_shell(stdout=True, stderr=True)

params = snakemake.params.get("extra", "")

read_fmt = snakemake.params.get("read_fmt", "paired")

memory = snakemake.params.get("memory", "8")
memory = "-m {memory}".format(memory=memory)

tmpdir = snakemake.params.get("tmpdir", "")
if len(tmpdir) > 0:
    tmpdir = "--tmp-dir {tmpdir}".format(tmpdir=tmpdir)

covcutoff = snakemake.params.get("covcutoff", "off")
covcutoff = "--cov-cutoff {covcutoff}".format(covcutoff=covcutoff)

if read_fmt == "paired":
    r1 = snakemake.input.get('r1', snakemake.input[0])
    r2 = snakemake.input.get('r2', snakemake.input[1])
    reads = "-1 {r1} -2 {r2}".format(r1=r1, r2=r2)
elif read_fmt == "merged":
    r12 = snakemake.input.get('r12', snakemake.input[0])
    reads = "--12 {r12}".format(r12=r12)
elif read_fmt == 'single':
    rs = snakemake.input.get('rs', snakemake.input[0])
    reads = "-s {rs}".format(rs=rs)

spades_dir = snakemake.output.get(
    "outdir", "spades")
outdir_opt = "-o {outdir}".format(outdir=spades_dir)

shell(
    "spades.py {params} {tmpdir} {memory} -t {snakemake.threads} {covcutoff} {outdir_opt} {reads} {log}")

spades_dir = pathlib.Path(spades_dir)
outdir = pathlib.Path(snakemake.output[0]).parent
for fn in snakemake.output:
    dest = pathlib.Path(fn)
    src = spades_dir / dest.name
    shutil.copyfile(str(src), str(dest))
